package com.love2learn.aop.aspectorder.pointcut.dao;

public interface PointCutAccountDao {

    public void createAccount();

    void createAccount(int accountNumber);
}
