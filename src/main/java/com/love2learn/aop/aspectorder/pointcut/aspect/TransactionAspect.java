package com.love2learn.aop.aspectorder.pointcut.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(1)
public class TransactionAspect {

    @Pointcut("execution(public void *(..))")
    public void transactionAspect(){}

    @Before("transactionAspect()")
    public void beforeAdvice(){
        System.out.println("This advice will apply transaction");
    }

}
