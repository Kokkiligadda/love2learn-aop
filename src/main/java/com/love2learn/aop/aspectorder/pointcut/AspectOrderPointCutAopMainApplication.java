package com.love2learn.aop.aspectorder.pointcut;

import com.love2learn.aop.aspectorder.pointcut.config.PointCutAopDemo;
import com.love2learn.aop.aspectorder.pointcut.dao.PointCutAccountDao;
import com.love2learn.aop.aspectorder.pointcut.dao.PointCutAccountDaoImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AspectOrderPointCutAopMainApplication {

    public static void main(String[] args) {
        //Create A Context
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(PointCutAopDemo.class);
        //Get Bean From Context
        PointCutAccountDao pointCutAccountDao = applicationContext.getBean(PointCutAccountDao.class);

        //Call a method
        System.out.println("Handling Account ................");
        System.out.println("create is called..........");
        pointCutAccountDao.createAccount();
        System.out.println("createAccount(int) is called..........");
        pointCutAccountDao.createAccount(123456);

        System.out.println("\n\n");

        //Close context
        applicationContext.close();
    }
}
