package com.love2learn.aop.aspectorder.pointcut.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(3)
public class LoggingAspect {

    @Pointcut("execution(public void *(..))")
    public void loggingAspect(){}

    @Before("loggingAspect()")
    public void beforeAdvice(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        for(Object obj : args){
            System.out.println("value ...."+(int)obj);
        }
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        System.out.println("get method ...."+signature.getMethod());
        System.out.println("Declaring type.... "+signature.getDeclaringType());
        System.out.println("Parameter Names.... "+signature.getParameterNames());
        System.out.println("return type ...."+signature.getReturnType());
        System.out.println("This advice will apply logging");
    }

}
