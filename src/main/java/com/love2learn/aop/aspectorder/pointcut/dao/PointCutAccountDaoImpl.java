package com.love2learn.aop.aspectorder.pointcut.dao;

import org.springframework.stereotype.Component;

@Component
public class PointCutAccountDaoImpl implements PointCutAccountDao{

    public void createAccount() {
        System.out.println("This is to createAccount ...............");
    }

    @Override
    public void createAccount(int accountNumber) {
        System.out.println("This is to createAccount using account number ............... ");
    }

}
