package com.love2learn.aop.afterreturn.dao;

import com.love2learn.aop.afterreturn.model.Account;

import java.util.List;

public interface AccountDao {
    List<Account> findAccountDetails();
}
