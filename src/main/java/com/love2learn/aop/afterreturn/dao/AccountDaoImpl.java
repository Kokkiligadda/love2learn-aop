package com.love2learn.aop.afterreturn.dao;

import com.love2learn.aop.afterreturn.model.Account;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class AccountDaoImpl implements AccountDao {
    @Override
    public List<Account> findAccountDetails() {
        Account accountOne = new Account(1234, "Bhagat");
        Account accountTwo = new Account(4321, "Singh");
        return Arrays.asList(accountOne,accountTwo);
    }
}
