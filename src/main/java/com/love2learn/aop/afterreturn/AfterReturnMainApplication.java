package com.love2learn.aop.afterreturn;

import com.love2learn.aop.afterreturn.config.AfterAOPConfig;
import com.love2learn.aop.afterreturn.dao.AccountDao;
import com.love2learn.aop.afterreturn.model.Account;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class AfterReturnMainApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(AfterAOPConfig.class);
        AccountDao accountDao = applicationContext.getBean(AccountDao.class);
        List<Account> accountDetails = accountDao.findAccountDetails();
        accountDetails.iterator().forEachRemaining(item -> {
            System.out.println(item.getAccountName() +" "+ item.getAccountNumber());
        });

    }
}
