package com.love2learn.aop.afterreturn.aspect;

import com.love2learn.aop.afterreturn.model.Account;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.List;

@Aspect
@Component
public class AfterReturningAspect {

    @Pointcut("execution(* findAccountDetails())")
    public void pointcutForAfterReturn(){

    }

    @After(value = "pointcutForAfterReturn()")
    public void afterAdvice(){
        System.out.println("This is after advice ....");
    }

    @AfterReturning(
            pointcut = "pointcutForAfterReturn()",
            returning = "result"
    )
    public void afterReturnAdvice(JoinPoint joinPoint, List<Account> result){
        Signature signature = joinPoint.getSignature();
        System.out.println("Get Method Name ..."+signature.getName());
        System.out.println("Advice is applied after return with point cut---------");
        System.out.println("print data in advice ..........");
        result.iterator().forEachRemaining(item -> {
            System.out.println(item.getAccountNumber()+" "+ item.getAccountName());
            item.setAccountName("Kokkiligadda");
            item.setAccountNumber(11111);
        });
        System.out.println("printing is completed in advice ..........");
    }
}
