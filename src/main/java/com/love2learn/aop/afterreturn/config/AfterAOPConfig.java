package com.love2learn.aop.afterreturn.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.love2learn.aop.afterreturn")
public class AfterAOPConfig {
}
