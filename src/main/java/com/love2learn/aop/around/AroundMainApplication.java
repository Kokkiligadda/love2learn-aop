package com.love2learn.aop.around;

import com.love2learn.aop.around.config.AroundAopConfig;
import com.love2learn.aop.around.dao.AccountDao;
import com.love2learn.aop.around.model.Account;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class AroundMainApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(AroundAopConfig.class);
        AccountDao accountDao = applicationContext.getBean(AccountDao.class);
        List<Account> accountDetails = accountDao.findAccountDetails();
        accountDao.findAccountDetailsException();
    }
}
