package com.love2learn.aop.around.aspect;

import com.love2learn.aop.around.model.Account;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Aspect
@Component
public class AroundAspect {

    @Pointcut("execution(* findAccountDetails())")
    public void pointcutForAroundAspect(){

    }

    @Pointcut("execution(* findAccountDetailsException())")
    public void pointcutForAroundAspectHandleException(){

    }

    @Around(value = "pointcutForAroundAspect()")
    public void afterAdvice(ProceedingJoinPoint proceedingJoinPoint){
        long begin = System.currentTimeMillis();
        System.out.println("This is after advice ....");
        long end = System.currentTimeMillis();
        System.out.println("Duration ....."+String.valueOf(end-begin));
    }

    @Around(value = "pointcutForAroundAspectHandleException()")
    public Object afterAdviceException(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        Object result = null;
        try {
            result = proceedingJoinPoint.proceed();
        }catch (Throwable e){
            System.out.println(e);
            throw e;
        }
        return result;
    }

}
