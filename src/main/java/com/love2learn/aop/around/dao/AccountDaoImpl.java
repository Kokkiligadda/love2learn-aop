package com.love2learn.aop.around.dao;

import com.love2learn.aop.around.model.Account;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class AccountDaoImpl implements AccountDao {
    @Override
    public List<Account> findAccountDetails() {
        Account accountOne = new Account(1234, "Bhagat");
        Account accountTwo = new Account(4321, "Singh");
        return Arrays.asList(accountOne,accountTwo);
    }

    @Override
    public void findAccountDetailsException() {
        throw new RuntimeException("Exception.....");
    }
}
