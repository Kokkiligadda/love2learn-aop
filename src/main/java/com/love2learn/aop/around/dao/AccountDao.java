package com.love2learn.aop.around.dao;

import com.love2learn.aop.around.model.Account;

import java.util.List;

public interface AccountDao {
    List<Account> findAccountDetails();

    void findAccountDetailsException();
}
