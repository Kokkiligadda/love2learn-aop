package com.love2learn.aop.pointcut.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class PointCutDeclarationAspect {

    @Pointcut("execution(public void *(..))")
    public void pointCutDeclaration(){}

    @Pointcut("execution(* set*(..))")
    public void pointCutForSetter(){}

    @Pointcut("execution(* get*(..))")
    public void pointCutForGetter(){}

    @Before("pointCutDeclaration()")
    public void beforeAdvice(){
        System.out.println("This advice will apply using pointcut declaration");
    }

    @Before("!(pointCutForSetter() || pointCutForGetter())")
    public void beforeAdviceNotGetterAndSetter(){
        System.out.println("This will not apply to Setters and Getters");

    }
}
