package com.love2learn.aop.pointcut;

import com.love2learn.aop.pointcut.config.PointCutAopDemo;
import com.love2learn.aop.pointcut.dao.PointCutAccountDaoImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PointCutDeclarationMainApplication {

    public static void main(String[] args) {
        //Create A Context
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(PointCutAopDemo.class);
        //Get Bean From Context
        PointCutAccountDaoImpl pointCutAccountDaoImpl = applicationContext.getBean(PointCutAccountDaoImpl.class);

        //Call a method
        System.out.println("Handling Account ................");
        System.out.println("setters are called..........");
        pointCutAccountDaoImpl.setAccountNumber("12345");
        pointCutAccountDaoImpl.setUserName("Bhagat");
        System.out.println("getters are called..........");
        pointCutAccountDaoImpl.getAccountNumber();
        pointCutAccountDaoImpl.getUserName();
        System.out.println("create is called..........");
        pointCutAccountDaoImpl.createAccount();

        System.out.println("\n\n");

        //Close context
        applicationContext.close();
    }
}
