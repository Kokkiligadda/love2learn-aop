package com.love2learn.aop.pointcut.dao;

import org.springframework.stereotype.Component;

@Component
public class PointCutAccountDaoImpl{

    public void createAccount() {
        System.out.println("This is to createAccount ...............");
    }
    private String userName;
    private String accountNumber;

    public String getUserName() {
        System.out.println("getUserName is called ..................");
        return userName;
    }

    public void setUserName(String userName) {
        System.out.println("setUserName is called ..................");
        this.userName = userName;
    }

    public String getAccountNumber() {
        System.out.println("getAccountNumber is called ...................");
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        System.out.println("setAccountNumber is called .....................");
        this.accountNumber = accountNumber;
    }

}
