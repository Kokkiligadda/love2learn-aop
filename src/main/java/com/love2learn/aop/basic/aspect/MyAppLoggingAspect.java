package com.love2learn.aop.basic.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MyAppLoggingAspect {

    //Match on method name
    @Before("execution(public void createAccount())")
    public void beforeAdvice(){
        System.out.println("This is my before advice logging .....");
    }

    //Match on Type and method (ClassName and Method Name)
    @Before("execution(public void com.love2learn.aop.basic.dao.AccountDao.createAccount())")
    public void beforeAdviceOnTypeAndMethod(){
        System.out.println("This is my before advice logging for Only Account.....");
    }

    //Match on method name starts with create in any class
    @Before("execution(public void create*())")
    public void beforeAdviceWildCard(){
        System.out.println("This is my before advice logging method name starts with create*.....");
    }

    //Match on any return type and method name starts with create
    @Before("execution(public * create*())")
    public void beforeAdviceAnyReturn(){
        System.out.println("This is my before advice logging with any return type and no arguments.....");
    }

    //Match on any return type and method name starts with create and any arguments
    @Before("execution(public * create*(..))")
    public void beforeAdviceAnyReturnAnyArgs(){
        System.out.println("This is my before advice logging with any return type and any arguments.....");
    }

    //Match on any return type and method name starts with create and one argument
    @Before("execution(public * create*(*))")
    public void beforeAdviceAnyReturnAndOneArg(){
        System.out.println("This is my before advice logging with any return type and one argument.....");
    }

    //Match on any return type and method name starts with create and String parameter
    @Before("execution(public * create*(String))")
    public void beforeAdviceAnyReturnAndSpecificParam(){
        System.out.println("This is my before advice logging with any return type and specific param.....");
    }

    //Match all methods in the package
    @Before("execution(* com.love2learn.aop.basic.dao.*.*(..))")
    public void beforeAdviceInPackage(){
        System.out.println("This is my before advice logging with any class in the package.....");
    }
}
