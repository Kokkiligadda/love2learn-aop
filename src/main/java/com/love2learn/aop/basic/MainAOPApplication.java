package com.love2learn.aop.basic;

import com.love2learn.aop.basic.config.AOPDemo;
import com.love2learn.aop.basic.dao.AccountDao;
import com.love2learn.aop.basic.dao.MembershipDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainAOPApplication {

    public static void main(String[] args) {
        //Create A Context
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(AOPDemo.class);
        //Get Bean From Context
        AccountDao accountDao = applicationContext.getBean(AccountDao.class);
        MembershipDao membershipDao = applicationContext.getBean(MembershipDao.class);

        //Call a method
        System.out.println("Handling Account ................");
        accountDao.createAccount();
        accountDao.createAccount((int)(1000*Math.random()));
        accountDao.createAccount("Bhagat");

        System.out.println("\n\n");

        System.out.println("Handling Membership..............");
        membershipDao.createAccount();

        //Close context
        applicationContext.close();
    }
}
