package com.love2learn.aop.basic.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDaoImpl implements MembershipDao {
    public void createAccount() {
        System.out.println(getClass()+ ":::::This is to create membership .......");
    }
}
