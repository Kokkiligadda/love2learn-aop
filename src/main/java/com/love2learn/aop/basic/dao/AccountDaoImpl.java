package com.love2learn.aop.basic.dao;

import org.springframework.stereotype.Component;

@Component
public class AccountDaoImpl implements AccountDao {

    public void createAccount() {
        System.out.println(getClass() + ":::::This is to create an account .....");
    }

    public boolean createAccount(int accountNumber) {
        System.out.println(getClass() + ":::::This is to create new account which returns boolean.....");
        return true;
    }

    public boolean createAccount(String accountNumber) {
        System.out.println(getClass() + ":::::This is to create new account which returns boolean and takes String.....");
        return true;
    }
}
