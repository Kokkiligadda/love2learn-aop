package com.love2learn.aop.basic.dao;

public interface AccountDao {
    void createAccount();

    boolean createAccount(int accountNumber);

    boolean createAccount(String accountNumber);
}


