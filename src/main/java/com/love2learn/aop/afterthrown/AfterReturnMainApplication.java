package com.love2learn.aop.afterthrown;

import com.love2learn.aop.afterthrown.config.AfterAOPConfig;
import com.love2learn.aop.afterthrown.dao.AccountDao;
import com.love2learn.aop.afterthrown.model.Account;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class AfterReturnMainApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(AfterAOPConfig.class);
        AccountDao accountDao = applicationContext.getBean(AccountDao.class);
        try{
            List<Account> accountDetails = accountDao.findAccountDetails();
        }catch (Exception exeption){
            System.out.println("Exception in main class");
        }
    }
}
