package com.love2learn.aop.afterthrown.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AfterReturningAspect {

    @Pointcut("execution(* findAccountDetails())")
    public void pointcutForAfterReturn(){

    }

    @AfterThrowing(
            pointcut = "pointcutForAfterReturn()",
            throwing = "exception"
    )
    public void afterReturnAdvice(JoinPoint joinPoint, Throwable exception){
        System.out.println("This is exception"+ exception);
    }
}
