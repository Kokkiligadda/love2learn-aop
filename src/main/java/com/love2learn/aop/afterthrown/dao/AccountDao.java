package com.love2learn.aop.afterthrown.dao;

import com.love2learn.aop.afterthrown.model.Account;

import java.util.List;

public interface AccountDao {
    List<Account> findAccountDetails();
}
